import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.types.StructType
import com.nttdata.gitpod_spark_poc.reader.TextReader
import com.nttdata.gitpod_spark_poc.processor.TextProcessor
import com.nttdata.gitpod_spark_poc.analyzer.TextAnalyzer
import com.nttdata.gitpod_spark_poc.saver.TextSaver

object Application {

    def main(args: Array[String]): Unit = {
        
        val textDs = TextReader.readText

        val processedDs = TextProcessor.processText(textDs)

        TextAnalyzer.analyzeText(processedDs, 15)

        TextSaver.save(processedDs)
    }
    
}