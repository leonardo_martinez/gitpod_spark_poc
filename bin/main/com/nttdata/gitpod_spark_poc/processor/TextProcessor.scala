package com.nttdata.gitpod_spark_poc.processor

import org.apache.spark.sql.Dataset
import org.slf4j.LoggerFactory

object TextProcessor {

    private val nonAlphanumericRegex = "([^A-Za-z0-9 ])*"

    private val logger = LoggerFactory.getLogger(TextProcessor.getClass())

    def processText(ds: Dataset[String]): Dataset[(String, Int)] = {
        import ds.sparkSession.implicits._

        logger.info("Processing text")

        ds.map(line => line.replaceAll(nonAlphanumericRegex, ""))
            .flatMap(line => line.split(" "))
            .filter(!_.equals(""))
            .map(word => (word, 1))
            .groupByKey(_._1)
            .reduceGroups((tuple1, tuple2) => (tuple1._1, tuple1._2 + tuple2._2))
            .map { case (key, tuple) => (key, tuple._2)}
    }
  
}
