package com.nttdata.gitpod_spark_poc.analyzer

import org.slf4j.LoggerFactory
import org.apache.spark.sql.Dataset
import org.apache.spark.sql.functions._

object TextAnalyzer {

    private val logger = LoggerFactory.getLogger(TextAnalyzer.getClass())

    def analyzeText(ds: Dataset[(String, Int)], mostFrequentNumber: Int = 10, frequencyCutoff: Int = 100): Unit = {
        import ds.sparkSession.implicits._

        val dsCache = ds.filter(tuple => tuple._2 >= frequencyCutoff)
            .cache()

        val wordCount = dsCache.count
        val mostFrequent = dsCache.sort(desc("_2"))
            .head(mostFrequentNumber)

        val mostFrequentProperNouns = dsCache.filter(tuple => tuple._1.head.isUpper)
            .sort(desc("_2"))
            .head(mostFrequentNumber)   

        logger.warn(s"Text has $wordCount unique words with frequency $frequencyCutoff or higher")
        
        logger.warn(s"The $mostFrequentNumber most frequent words are: \n${mostFrequent.mkString("\n")}")

        logger.warn(s"The most frequent proper nouns are: \n${mostFrequentProperNouns.mkString("\n")}")

    }
  
}
