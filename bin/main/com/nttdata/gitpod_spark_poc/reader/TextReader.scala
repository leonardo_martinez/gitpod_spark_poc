package com.nttdata.gitpod_spark_poc.reader

import org.apache.spark.sql.Dataset
import org.apache.spark.sql.SparkSession
import org.slf4j.LoggerFactory

object TextReader {

    private val spark = SparkSession.builder().appName("gitpod_spark_poc")
            .master("local[*]")
            .getOrCreate()

    private val logger = LoggerFactory.getLogger(TextReader.getClass())

    import spark.implicits._
    
    def readText: Dataset[String] = {
        logger.info("Reading text")

        val df = spark.read.text("src/main/resources/War and Peace.txt")

        df.as[String]
    }
  
}
