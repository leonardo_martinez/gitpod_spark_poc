package com.nttdata.gitpod_spark_poc.saver

import org.apache.spark.sql.Dataset
import org.apache.spark.sql.SaveMode

object TextSaver {

    def save(ds: Dataset[(String, Int)]): Unit = {
        import ds.sparkSession.implicits._

        ds.coalesce(1)
            .write
            .mode(SaveMode.Overwrite)
            .csv("./saved/")
    }
  
}
